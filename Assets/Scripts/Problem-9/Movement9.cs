﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement9 : MonoBehaviour
{
    public float speed;
    Vector2 constant;
    public Rigidbody2D rig;

    Vector3 MousePosition;
    Vector3 arah;

    bool inWall;
    bool firstWay = false;

    public GameObject gameover;
    // Start is called before the first frame update
    void Start()
    {
        rig = GetComponent<Rigidbody2D>();

        Time.timeScale = 1;
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButton(0))
        {
            if (firstWay == false)
            {
                MousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                arah = (MousePosition - transform.position).normalized;
                rig.velocity = new Vector2(arah.x * speed, arah.y * speed);
                firstWay = true;
            }
            if (inWall == true)
            {
                MousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                arah = (MousePosition - transform.position).normalized;
                rig.velocity = new Vector2(arah.x * speed, arah.y * speed);

            }


        }
        else if (Input.GetMouseButton(0) && inWall == true)
        {
            rig.velocity = new Vector2(arah.x * speed, arah.y * speed);
        }
        else
        {
            var jalan = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0);


            transform.position += jalan * speed * Time.deltaTime;
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Wall")
        {
            rig.velocity = Vector2.zero;
            inWall = true;
        }
        else
        {
            inWall = false;
        }
        if(collision.gameObject.tag == "Enemy")
        {
            gameover.SetActive(true);
            Time.timeScale = 0;
        }
    }
}
