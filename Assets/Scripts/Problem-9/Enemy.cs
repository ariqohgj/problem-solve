﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float speed = 7f;
    bool rightSide = true;
    public float range = 14f;

    private SpriteRenderer EnemyRenderer;
    // Start is called before the first frame update
    void Start()
    {
        EnemyRenderer = gameObject.GetComponent<SpriteRenderer>();
        EnemyRenderer.flipX = rightSide;
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 direction = (rightSide) ? transform.right : -transform.right;
        direction *= Time.deltaTime * speed;

        transform.Translate(direction);

        CheckWall();

   
    }

    void CheckWall()
    {
        Vector3 raycastDirect = (rightSide) ? Vector3.right : -Vector3.right;

        RaycastHit2D hit = Physics2D.Raycast(transform.position + raycastDirect * range - new Vector3(0f, 0.25f, 0f), raycastDirect, 0.075f);

        if(hit.collider != null)
        {
            if(hit.transform.tag == "Wall")
            {
                rightSide = !rightSide;
                EnemyRenderer.flipX = rightSide;
            }
        }
    }
}
