﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public float speed;
    Vector3 valocity;
    Vector2 constant;
    public Rigidbody2D rig;
    // Start is called before the first frame update
    void Start()
    {
        rig = GetComponent<Rigidbody2D>();
        constant = new Vector2(speed, 10);
        
        rig.AddForce(constant);

    }

    // Update is called once per frame
    void Update()
    {

        rig.velocity = constant * (rig.velocity.normalized);


        valocity = rig.velocity;
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        var direction = Vector3.Reflect(valocity.normalized, collision.contacts[0].normal);

        rig.velocity = direction * Mathf.Max(speed, 0f);
    }

}
