﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement5 : MonoBehaviour
{
    public float speed;
    Vector2 constant;
    public Rigidbody2D rig;

    Vector3 MousePosition;
    Vector3 arah;
    // Start is called before the first frame update
    void Start()
    {
        rig = GetComponent<Rigidbody2D>();
        constant = new Vector2(speed, 0);


    }

    // Update is called once per frame
    void Update()
    {
        var jalan = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0);
        

        transform.position += jalan * speed * Time.deltaTime;
        if (Input.GetMouseButton(0))
        {
            MousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            arah = (MousePosition - transform.position).normalized;
            rig.velocity = new Vector2(arah.x *speed, arah.y*speed );
          
        }
        else
        {
            rig.velocity = new Vector2(arah.x * speed, arah.y * speed);
        }
        
    }
}
