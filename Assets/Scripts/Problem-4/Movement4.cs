﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement4 : MonoBehaviour
{
    public float speed;
    Vector2 constant;
    public Rigidbody2D rig;
    // Start is called before the first frame update
    void Start()
    {
        rig = GetComponent<Rigidbody2D>();
        constant = new Vector2(speed, 0);
    }

    // Update is called once per frame
    void Update()
    {
        var jalan = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0);

        transform.position += jalan * speed * Time.deltaTime;
    }
}
