﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class kotak8 : MonoBehaviour
{
    SpawnSprite8 spawn;
    // Start is called before the first frame update
    void Start()
    {
        spawn = GameObject.Find("Spawn Manager").GetComponent<SpawnSprite8>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(gameObject);
        Score8.score++;
        spawn.spwanitNow();
    }
}
