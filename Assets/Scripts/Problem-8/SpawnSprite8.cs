﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnSprite8 : MonoBehaviour
{
    public Transform kotak;
    public SpriteRenderer sr;
    

    // Start is called before the first frame update
    void Start()
    {
      
        for (int i = 0; i < 5; i++)
        {
            float spawnY = Random.Range
                (Camera.main.ScreenToWorldPoint(new Vector2(0, 0)).y, Camera.main.ScreenToWorldPoint(new Vector2(0, Screen.height)).y);
            float spawnX = Random.Range
                (Camera.main.ScreenToWorldPoint(new Vector2(0, 0)).x, Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, 0)).x);

            Vector2 spawnPosition = new Vector2(spawnX, spawnY);
            Instantiate(kotak, spawnPosition, Quaternion.identity);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator spawninthreesec()
    {
      
        float spawnY = Random.Range
        (Camera.main.ScreenToWorldPoint(new Vector2(0, 0)).y, Camera.main.ScreenToWorldPoint(new Vector2(0, Screen.height)).y);
        float spawnX = Random.Range
        (Camera.main.ScreenToWorldPoint(new Vector2(0, 0)).x, Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, 0)).x);
        Vector2 spawnPosition = new Vector2(spawnX, spawnY + sr.bounds.size.y );

        yield return new WaitForSecondsRealtime(3f);
        Instantiate(kotak, spawnPosition, Quaternion.identity);




    }

    public void spwanitNow()
    {
        StartCoroutine(spawninthreesec());
    }
}
